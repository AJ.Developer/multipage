﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MultiPage.DataLayer
{
    [Table("IngredientUnits")]
    public class IngredientUnit : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch(columnName)
                {
                    case "RequiredQuantity":
                        if (this.RequiredQuantity < 1)
                            return "Minimum RequiredQuantity Is 1";
                        break;
                    case "IngredientID":
                        if ( this.IngredientID < 1)
                            return "Ingredient Is Required";
                        break;
                    default: return string.Empty;

                }
                return string.Empty;
            }
        }

        [Key]
        public long IngredientUnitID { get; set; }

        [Required]
        public int RequiredQuantity { get; set; }

        //Ingredient

        public long? IngredientID { get; set; }

        [ForeignKey("IngredientID")]
        public virtual Ingredient Ingredient { get; set; }

        //StockItems
        public long? StockItemID { get; set; }
        [ForeignKey("StockItemID")]
        public virtual StockItem StockItem { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
