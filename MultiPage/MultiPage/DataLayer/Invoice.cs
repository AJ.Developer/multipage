﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MultiPage.DataLayer
{
    [Table("Invoices")]
    public class Invoice
    {
        [Key]
        public long InvoiceID { get; set; }
        public Invoice()
        {
            InvoiceItems = new HashSet<InvoiceItem>();
        }
        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        public InvoiceStatus InvoiceStatus { get; set; }

        public DateTime? Date { get; set; }

        [NotMapped]
        public string FormattedDate
        {
            get
            {
                if (Date != null)
                    return Date.Value.ToString("ddd, dd MMM hh:mm tt");
                else
                    return "";
            }
        }
        [NotMapped]
        public string FilterDate
        {
            get
            {
                if (Date != null)
                    return Date.Value.ToString("ddd, dd MMM") + " ";
                else
                    return "";
            }
        }
        //Client
        public long? ClientID { get; set; }

        [ForeignKey("ClientID")]
        public virtual Client Client { get; set; }

        [NotMapped]
        public decimal TotalAmount
        {
            get
            {
                if (this.InvoiceItems != null)
                {
                    decimal amount = 0;
                    foreach (InvoiceItem item in InvoiceItems)
                        amount += ((item.Quantity + item.PaidQuantity) * item.UnitPrice);
                    return amount;
                }
                else
                    return 0;
            }
        }
        [NotMapped]
        public decimal RestAmount
        {
            get
            {
                if (this.InvoiceItems != null)
                {
                    decimal amount = 0;
                    foreach (InvoiceItem item in InvoiceItems)
                        amount += item.TotalPrice;
                    return amount;
                }
                else
                    return 0;
            }
        }

    }
}
