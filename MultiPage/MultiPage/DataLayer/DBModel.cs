namespace MultiPage.DataLayer
{
    using System.Data.Entity;
    public class DBModel : DbContext
    {
        public DBModel()
            : base(nameOrConnectionString: "Default")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DBModel, MultiPage.Migrations.Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {                       
            base.OnModelCreating(modelBuilder);
        }
        public virtual DbSet<Configuration> Configurations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceItem> InvoiceItems { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<StockItem> StockItems { get; set; }
        public DbSet<IngredientUnit> IngredientUnits { get; set; }
        public DbSet<SaleItem> SaleItems { get; set; }
        public DbSet<SaleUnit> SaleUnits { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}