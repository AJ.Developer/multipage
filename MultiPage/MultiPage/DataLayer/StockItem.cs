﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace MultiPage.DataLayer
{
    [Table("StockItems")]
    public class StockItem : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Price":
                        if (this.Quantity < 0)
                            return "Minimum Quantity Is 0";
                        break;
                    case "Name":
                        if (string.IsNullOrEmpty(this.Name))
                            return "Name Is Required";
                        break;
                    case "PurchasePrice":
                        if (this.PurchasePrice < 0)
                            return "Minimum PurchasePrice Is 0 LBP";
                        break;
                    case "SalePrice":
                        if (this.SalePrice < 250)
                            return "Minimum SalePrice Is 250 LBP";
                        break;
                    case "CategoryID":
                        if (this.CategoryID < 1)
                            return "Category Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }

        [Key]
        public long StockItemID { get; set; }
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public decimal PurchasePrice { get; set; }
        [Required]
        public decimal SalePrice { get; set; }
        [Required]
        public int Quantity { get; set; }
        //Category
        public long? CategoryID { get; set; }
        [ForeignKey("CategoryID")]
        public virtual Category Category { get; set; }
        public virtual ICollection<IngredientUnit> IngredientUnits { get; set; }
        public virtual ICollection<SaleUnit> SaleUnits { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
