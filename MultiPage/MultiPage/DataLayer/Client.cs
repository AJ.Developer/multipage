﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultiPage.DataLayer
{
    [Table("Clients")]
    public class Client : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Name") && string.IsNullOrEmpty(this.Name))
                    return "Name Is Required";
                return string.Empty;
            }
        }

        [Key]
        public long ClientID { get; set; }

        [Required]
        public virtual string Name { get; set; }
        public ClientLocation ClientLocation { get; set; }
        public string Address { get; set; }

        public string Phone { get; set; }
        public virtual ICollection<Invoice> Invoice { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
