﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MultiPage.DataLayer
{
    [Table("SaleItems")]
    public class SaleItem : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Price":
                        if (this.Price < 250)
                            return "Minimum Price Is 250 LBP";
                        break;
                    case "Name":
                        if (string.IsNullOrEmpty(this.Name))
                            return "Name Is Required";
                        break;
                    case "UserID":
                        if (this.UserID < 1)
                            return "Seller Is Required";
                        break;
                    case "CategoryID":
                        if (this.CategoryID < 1)
                            return "Category Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }

        [Key]
        public long SaleItemID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }

        public bool IsAvailable { get; set; }

        public string BackGround { get; set; }

        public string ForeGround { get; set; }

        //User
        public long? UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual User User { get; set; }
        //Category
        public long? CategoryID { get; set; }
        [ForeignKey("CategoryID")]
        public virtual Category Category { get; set; }
        public virtual ICollection<SaleUnit> SaleUnits { get; set; }

        public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
