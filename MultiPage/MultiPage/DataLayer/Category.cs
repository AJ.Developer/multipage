﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultiPage.DataLayer
{
    [Table("Categories")]
    public class Category : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                return "";
                //try
                //{
                //    if (columnName.Equals("Name") && string.IsNullOrEmpty(this.Name))
                //        return "Name Is Required";
                //    return string.Empty;
                //}
                //catch (Exception e)
                //{
                //    return "";
                //}
            }
        }

        [Key]
        public long CategoryID { get; set; }

        [Required]
        public virtual string Name { get; set; }
        public bool IsConsumable { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
