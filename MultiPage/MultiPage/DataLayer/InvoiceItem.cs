﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MultiPage.DataLayer
{
    [Table("InvoiceItems")]
    public class InvoiceItem
    {
        [Key]
        public long InvoiceItemID { get; set; }

        [Required]
        public int Quantity { get; set; }

        public int PaidQuantity { get; set; }
        //SaleItem
        public long? SaleItemID { get; set; }
        [ForeignKey("SaleItemID")]
        public virtual SaleItem SaleItem { get; set; }

        //Invoice
        public long? InvoiceID { get; set; }
        [ForeignKey("InvoiceID")]
        public virtual Invoice Invoice { get; set; }

        public override string ToString()
        {
            try
            {
                return (Quantity + PaidQuantity) + " " + SaleItem.Name;
            }
            catch (Exception)
            {
                return "";
            }
        }

        [NotMapped]
        public virtual decimal UnitPrice
        {
            get
            {
                if (SaleItem != null)
                    return SaleItem.Price;
                else
                    return 0;
            }
        }
        [NotMapped]
        public virtual decimal TotalPrice
        {
            get
            {
                if (SaleItem != null)
                    return SaleItem.Price * Quantity;
                else
                    return 0;
            }
        }
        [NotMapped]
        public virtual decimal PriceForCharts
        {
            get
            {
                if (SaleItem != null)
                    return SaleItem.Price * PaidQuantity;
                else
                    return 0;
            }
        }

    }
}
