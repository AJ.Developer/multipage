﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPage.DataLayer
{
    public enum QuantityUnit
    {
        Gram,
        MilliLitre,
        Piece
    }
}
