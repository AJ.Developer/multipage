﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiPage.DataLayer
{
    public enum InvoiceStatus
    {
        NOT_PAID,
        DONE,
        PAID_WAITING_FOR_RETURN,
        NOT_PAID_WAITING_FOR_RETURN,
        PAID_AND_EXPIRED,
        NOT_PAID_AND_EXPIRED
    }
}
