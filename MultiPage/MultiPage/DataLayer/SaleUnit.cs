﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MultiPage.DataLayer
{
    [Table("SaleUnits")]
    public class SaleUnit : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Quantity":
                        if (this.Quantity < 1)
                            return "Minimum Quantity Is 1";
                        break;
                    case "StockItemID":
                        if (this.StockItemID < 1)
                            return "Stock item Is Required";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }


        [Key]
        public long SaleUnitID { get; set; }

        public int Quantity { get; set; }

        //SaleItem
        public long? SaleItemID { get; set; }

        [ForeignKey("SaleItemID")]
        public virtual SaleItem SaleItem { get; set; }

        //StockItems
        public long? StockItemID { get; set; }
        [ForeignKey("StockItemID")]
        public virtual StockItem StockItem { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
