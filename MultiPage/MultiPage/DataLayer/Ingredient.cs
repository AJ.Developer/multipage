﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MultiPage.DataLayer
{
    [Table("Ingredients")]
    public class Ingredient : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "PurchasePriceOfAFullUnit":
                        if (this.PurchasePriceOfAFullUnit < 0)
                            return "Minimum Price Is 0 LBP";
                        break;
                    case "Name":
                        if (string.IsNullOrEmpty(this.Name))
                            return "Name Is Required";
                        break;
                    case "Quantity":
                        if (this.Quantity < 0)
                            return "Quantity Must Be MultiPageitive";
                        break;
                    default: return string.Empty;
                }
                return string.Empty;
            }
        }
        [Key]
        public long IngredientID { get; set; }
        [Required]
        public string Name { get; set; }
        public decimal PurchasePriceOfAFullUnit { get; set; }
        public int Quantity { get; set; }
        public QuantityUnit Unit { get; set; }
        public virtual ICollection<IngredientUnit> IngredientUnits { get; set; }

        [NotMapped]
        public string Error => "";
    }
}
