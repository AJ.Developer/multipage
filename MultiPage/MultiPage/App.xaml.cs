﻿using MultiPage.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
namespace MultiPage
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
       
            void App_Startup(object sender, StartupEventArgs e)
            {
                try
                {

                    ShutdownMode = ShutdownMode.OnLastWindowClose;
                    if (DateTime.Now.Year > 2020)
                        this.Shutdown();
                    //ApplicationThemeHelper.ApplicationThemeName = Theme.HybridApp.Name;
                    using (var context = new DBModel())
                    {
                        var Cars = context.Categories.ToArray();
                       
                    }
                    // Create main application window, starting minimized if specified
               
                        MainWindow main = new MainWindow();
                        main.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        main.WindowState = WindowState.Maximized;
                        main.Show();
                

                  

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    Current.Shutdown(0);
                }

            }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {

        }
    }
}
