﻿using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.EF6;
using MultiPage;
using MultiPage.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MultiPage.DBModelDataModel {

    /// <summary>
    /// A DBModelUnitOfWork instance that represents the run-time implementation of the IDBModelUnitOfWork interface.
    /// </summary>
    public class DBModelUnitOfWork : DbUnitOfWork<DBModel>, IDBModelUnitOfWork {

        public DBModelUnitOfWork(Func<DBModel> contextFactory)
            : base(contextFactory) {
        }

        IRepository<Category, long> IDBModelUnitOfWork.Categories {
            get { return GetRepository(x => x.Set<Category>(), (Category x) => x.CategoryID); }
        }

        IRepository<Client, long> IDBModelUnitOfWork.Clients {
            get { return GetRepository(x => x.Set<Client>(), (Client x) => x.ClientID); }
        }

        IRepository<Invoice, long> IDBModelUnitOfWork.Invoices {
            get { return GetRepository(x => x.Set<Invoice>(), (Invoice x) => x.InvoiceID); }
        }

        IRepository<InvoiceItem, long> IDBModelUnitOfWork.InvoiceItems {
            get { return GetRepository(x => x.Set<InvoiceItem>(), (InvoiceItem x) => x.InvoiceItemID); }
        }

        IRepository<SaleItem, long> IDBModelUnitOfWork.SaleItems {
            get { return GetRepository(x => x.Set<SaleItem>(), (SaleItem x) => x.SaleItemID); }
        }

        IRepository<SaleUnit, long> IDBModelUnitOfWork.SaleUnits {
            get { return GetRepository(x => x.Set<SaleUnit>(), (SaleUnit x) => x.SaleUnitID); }
        }

        IRepository<StockItem, long> IDBModelUnitOfWork.StockItems {
            get { return GetRepository(x => x.Set<StockItem>(), (StockItem x) => x.StockItemID); }
        }

        IRepository<IngredientUnit, long> IDBModelUnitOfWork.IngredientUnits {
            get { return GetRepository(x => x.Set<IngredientUnit>(), (IngredientUnit x) => x.IngredientUnitID); }
        }

        IRepository<Ingredient, long> IDBModelUnitOfWork.Ingredients {
            get { return GetRepository(x => x.Set<Ingredient>(), (Ingredient x) => x.IngredientID); }
        }

        IRepository<User, long> IDBModelUnitOfWork.Users {
            get { return GetRepository(x => x.Set<User>(), (User x) => x.UserID); }
        }

        IRepository<Configuration, int> IDBModelUnitOfWork.Configurations {
            get { return GetRepository(x => x.Set<Configuration>(), (Configuration x) => x.ID); }
        }
    }
}
