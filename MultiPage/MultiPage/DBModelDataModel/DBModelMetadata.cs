﻿using DevExpress.Mvvm.DataAnnotations;
using MultiPage;
using MultiPage.DataLayer;
using MultiPage.Localization;

namespace MultiPage.DBModelDataModel {

    public class DBModelMetadataProvider {
		        public static void BuildMetadata(MetadataBuilder<Category> builder) {
			builder.DisplayName(DBModelResources.Category);
						builder.Property(x => x.CategoryID).DisplayName(DBModelResources.Category_CategoryID);
						builder.Property(x => x.Name).DisplayName(DBModelResources.Category_Name);
						builder.Property(x => x.IsConsumable).DisplayName(DBModelResources.Category_IsConsumable);
			        }
		        public static void BuildMetadata(MetadataBuilder<Client> builder) {
			builder.DisplayName(DBModelResources.Client);
						builder.Property(x => x.ClientID).DisplayName(DBModelResources.Client_ClientID);
						builder.Property(x => x.Name).DisplayName(DBModelResources.Client_Name);
						builder.Property(x => x.ClientLocation).DisplayName(DBModelResources.Client_ClientLocation);
						builder.Property(x => x.Address).DisplayName(DBModelResources.Client_Address);
						builder.Property(x => x.Phone).DisplayName(DBModelResources.Client_Phone);
			        }
		        public static void BuildMetadata(MetadataBuilder<Invoice> builder) {
			builder.DisplayName(DBModelResources.Invoice);
						builder.Property(x => x.InvoiceID).DisplayName(DBModelResources.Invoice_InvoiceID);
						builder.Property(x => x.InvoiceStatus).DisplayName(DBModelResources.Invoice_InvoiceStatus);
						builder.Property(x => x.Date).DisplayName(DBModelResources.Invoice_Date);
						builder.Property(x => x.Client).DisplayName(DBModelResources.Invoice_Client);
			        }
		        public static void BuildMetadata(MetadataBuilder<InvoiceItem> builder) {
			builder.DisplayName(DBModelResources.InvoiceItem);
						builder.Property(x => x.InvoiceItemID).DisplayName(DBModelResources.InvoiceItem_InvoiceItemID);
						builder.Property(x => x.Quantity).DisplayName(DBModelResources.InvoiceItem_Quantity);
						builder.Property(x => x.PaidQuantity).DisplayName(DBModelResources.InvoiceItem_PaidQuantity);
						builder.Property(x => x.Invoice).DisplayName(DBModelResources.InvoiceItem_Invoice);
						builder.Property(x => x.SaleItem).DisplayName(DBModelResources.InvoiceItem_SaleItem);
			        }
		        public static void BuildMetadata(MetadataBuilder<SaleItem> builder) {
			builder.DisplayName(DBModelResources.SaleItem);
						builder.Property(x => x.SaleItemID).DisplayName(DBModelResources.SaleItem_SaleItemID);
						builder.Property(x => x.Name).DisplayName(DBModelResources.SaleItem_Name);
						builder.Property(x => x.Price).DisplayName(DBModelResources.SaleItem_Price);
						builder.Property(x => x.IsAvailable).DisplayName(DBModelResources.SaleItem_IsAvailable);
						builder.Property(x => x.BackGround).DisplayName(DBModelResources.SaleItem_BackGround);
						builder.Property(x => x.ForeGround).DisplayName(DBModelResources.SaleItem_ForeGround);
						builder.Property(x => x.Category).DisplayName(DBModelResources.SaleItem_Category);
						builder.Property(x => x.User).DisplayName(DBModelResources.SaleItem_User);
			        }
		        public static void BuildMetadata(MetadataBuilder<SaleUnit> builder) {
			builder.DisplayName(DBModelResources.SaleUnit);
						builder.Property(x => x.SaleUnitID).DisplayName(DBModelResources.SaleUnit_SaleUnitID);
						builder.Property(x => x.Quantity).DisplayName(DBModelResources.SaleUnit_Quantity);
						builder.Property(x => x.SaleItem).DisplayName(DBModelResources.SaleUnit_SaleItem);
						builder.Property(x => x.StockItem).DisplayName(DBModelResources.SaleUnit_StockItem);
			        }
		        public static void BuildMetadata(MetadataBuilder<StockItem> builder) {
			builder.DisplayName(DBModelResources.StockItem);
						builder.Property(x => x.StockItemID).DisplayName(DBModelResources.StockItem_StockItemID);
						builder.Property(x => x.Name).DisplayName(DBModelResources.StockItem_Name);
						builder.Property(x => x.PurchasePrice).DisplayName(DBModelResources.StockItem_PurchasePrice);
						builder.Property(x => x.SalePrice).DisplayName(DBModelResources.StockItem_SalePrice);
						builder.Property(x => x.Quantity).DisplayName(DBModelResources.StockItem_Quantity);
						builder.Property(x => x.Category).DisplayName(DBModelResources.StockItem_Category);
			        }
		        public static void BuildMetadata(MetadataBuilder<IngredientUnit> builder) {
			builder.DisplayName(DBModelResources.IngredientUnit);
						builder.Property(x => x.IngredientUnitID).DisplayName(DBModelResources.IngredientUnit_IngredientUnitID);
						builder.Property(x => x.RequiredQuantity).DisplayName(DBModelResources.IngredientUnit_RequiredQuantity);
						builder.Property(x => x.Ingredient).DisplayName(DBModelResources.IngredientUnit_Ingredient);
						builder.Property(x => x.StockItem).DisplayName(DBModelResources.IngredientUnit_StockItem);
			        }
		        public static void BuildMetadata(MetadataBuilder<Ingredient> builder) {
			builder.DisplayName(DBModelResources.Ingredient);
						builder.Property(x => x.IngredientID).DisplayName(DBModelResources.Ingredient_IngredientID);
						builder.Property(x => x.Name).DisplayName(DBModelResources.Ingredient_Name);
						builder.Property(x => x.PurchasePriceOfAFullUnit).DisplayName(DBModelResources.Ingredient_PurchasePriceOfAFullUnit);
						builder.Property(x => x.Quantity).DisplayName(DBModelResources.Ingredient_Quantity);
						builder.Property(x => x.Unit).DisplayName(DBModelResources.Ingredient_Unit);
			        }
		        public static void BuildMetadata(MetadataBuilder<User> builder) {
			builder.DisplayName(DBModelResources.User);
						builder.Property(x => x.UserID).DisplayName(DBModelResources.User_UserID);
						builder.Property(x => x.Username).DisplayName(DBModelResources.User_Username);
						builder.Property(x => x.Password).DisplayName(DBModelResources.User_Password);
						builder.Property(x => x.IsAdmin).DisplayName(DBModelResources.User_IsAdmin);
			        }
		        public static void BuildMetadata(MetadataBuilder<Configuration> builder) {
			builder.DisplayName(DBModelResources.Configuration);
						builder.Property(x => x.ID).DisplayName(DBModelResources.Configuration_ID);
						builder.Property(x => x.Key).DisplayName(DBModelResources.Configuration_Key);
						builder.Property(x => x.Value).DisplayName(DBModelResources.Configuration_Value);
			        }
		    }
}