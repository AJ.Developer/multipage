﻿using DevExpress.Mvvm.DataModel;
using MultiPage;
using MultiPage.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MultiPage.DBModelDataModel {

    /// <summary>
    /// IDBModelUnitOfWork extends the IUnitOfWork interface with repositories representing specific entities.
    /// </summary>
    public interface IDBModelUnitOfWork : IUnitOfWork {
        
        /// <summary>
        /// The Category entities repository.
        /// </summary>
		IRepository<Category, long> Categories { get; }
        
        /// <summary>
        /// The Client entities repository.
        /// </summary>
		IRepository<Client, long> Clients { get; }
        
        /// <summary>
        /// The Invoice entities repository.
        /// </summary>
		IRepository<Invoice, long> Invoices { get; }
        
        /// <summary>
        /// The InvoiceItem entities repository.
        /// </summary>
		IRepository<InvoiceItem, long> InvoiceItems { get; }
        
        /// <summary>
        /// The SaleItem entities repository.
        /// </summary>
		IRepository<SaleItem, long> SaleItems { get; }
        
        /// <summary>
        /// The SaleUnit entities repository.
        /// </summary>
		IRepository<SaleUnit, long> SaleUnits { get; }
        
        /// <summary>
        /// The StockItem entities repository.
        /// </summary>
		IRepository<StockItem, long> StockItems { get; }
        
        /// <summary>
        /// The IngredientUnit entities repository.
        /// </summary>
		IRepository<IngredientUnit, long> IngredientUnits { get; }
        
        /// <summary>
        /// The Ingredient entities repository.
        /// </summary>
		IRepository<Ingredient, long> Ingredients { get; }
        
        /// <summary>
        /// The User entities repository.
        /// </summary>
		IRepository<User, long> Users { get; }
        
        /// <summary>
        /// The Configuration entities repository.
        /// </summary>
		IRepository<Configuration, int> Configurations { get; }
    }
}
