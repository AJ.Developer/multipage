﻿using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.DesignTime;
using MultiPage;
using MultiPage.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MultiPage.DBModelDataModel {

    /// <summary>
    /// A DBModelDesignTimeUnitOfWork instance that represents the design-time implementation of the IDBModelUnitOfWork interface.
    /// </summary>
    public class DBModelDesignTimeUnitOfWork : DesignTimeUnitOfWork, IDBModelUnitOfWork {

        /// <summary>
        /// Initializes a new instance of the DBModelDesignTimeUnitOfWork class.
        /// </summary>
        public DBModelDesignTimeUnitOfWork() {
        }

        IRepository<Category, long> IDBModelUnitOfWork.Categories {
            get { return GetRepository((Category x) => x.CategoryID); }
        }

        IRepository<Client, long> IDBModelUnitOfWork.Clients {
            get { return GetRepository((Client x) => x.ClientID); }
        }

        IRepository<Invoice, long> IDBModelUnitOfWork.Invoices {
            get { return GetRepository((Invoice x) => x.InvoiceID); }
        }

        IRepository<InvoiceItem, long> IDBModelUnitOfWork.InvoiceItems {
            get { return GetRepository((InvoiceItem x) => x.InvoiceItemID); }
        }

        IRepository<SaleItem, long> IDBModelUnitOfWork.SaleItems {
            get { return GetRepository((SaleItem x) => x.SaleItemID); }
        }

        IRepository<SaleUnit, long> IDBModelUnitOfWork.SaleUnits {
            get { return GetRepository((SaleUnit x) => x.SaleUnitID); }
        }

        IRepository<StockItem, long> IDBModelUnitOfWork.StockItems {
            get { return GetRepository((StockItem x) => x.StockItemID); }
        }

        IRepository<IngredientUnit, long> IDBModelUnitOfWork.IngredientUnits {
            get { return GetRepository((IngredientUnit x) => x.IngredientUnitID); }
        }

        IRepository<Ingredient, long> IDBModelUnitOfWork.Ingredients {
            get { return GetRepository((Ingredient x) => x.IngredientID); }
        }

        IRepository<User, long> IDBModelUnitOfWork.Users {
            get { return GetRepository((User x) => x.UserID); }
        }

        IRepository<Configuration, int> IDBModelUnitOfWork.Configurations {
            get { return GetRepository((Configuration x) => x.ID); }
        }
    }
}
