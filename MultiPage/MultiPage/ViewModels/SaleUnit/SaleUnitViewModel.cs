﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single SaleUnit object view model.
    /// </summary>
    public partial class SaleUnitViewModel : SingleObjectViewModel<SaleUnit, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of SaleUnitViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static SaleUnitViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new SaleUnitViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the SaleUnitViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the SaleUnitViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected SaleUnitViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.SaleUnits, x => x.Error) {
                }


        /// <summary>
        /// The view model that contains a look-up collection of SaleItems for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<SaleItem> LookUpSaleItems {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (SaleUnitViewModel x) => x.LookUpSaleItems,
                    getRepositoryFunc: x => x.SaleItems);
            }
        }
        /// <summary>
        /// The view model that contains a look-up collection of StockItems for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<StockItem> LookUpStockItems {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (SaleUnitViewModel x) => x.LookUpStockItems,
                    getRepositoryFunc: x => x.StockItems);
            }
        }

    }
}
