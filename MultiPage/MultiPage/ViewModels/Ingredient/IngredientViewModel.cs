﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single Ingredient object view model.
    /// </summary>
    public partial class IngredientViewModel : SingleObjectViewModel<Ingredient, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of IngredientViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static IngredientViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new IngredientViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the IngredientViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the IngredientViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected IngredientViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Ingredients, x => x.Name) {
                }


        /// <summary>
        /// The view model that contains a look-up collection of IngredientUnits for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<IngredientUnit> LookUpIngredientUnits {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (IngredientViewModel x) => x.LookUpIngredientUnits,
                    getRepositoryFunc: x => x.IngredientUnits);
            }
        }


        /// <summary>
        /// The view model for the IngredientIngredientUnits detail collection.
        /// </summary>
        public CollectionViewModelBase<IngredientUnit, IngredientUnit, long, IDBModelUnitOfWork> IngredientIngredientUnitsDetails {
            get {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (IngredientViewModel x) => x.IngredientIngredientUnitsDetails,
                    getRepositoryFunc: x => x.IngredientUnits,
                    foreignKeyExpression: x => x.IngredientID,
                    navigationExpression: x => x.Ingredient);
            }
        }
    }
}
