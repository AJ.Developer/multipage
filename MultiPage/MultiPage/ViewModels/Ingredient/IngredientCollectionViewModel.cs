﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the Ingredients collection view model.
    /// </summary>
    public partial class IngredientCollectionViewModel : CollectionViewModel<Ingredient, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of IngredientCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static IngredientCollectionViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new IngredientCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the IngredientCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the IngredientCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected IngredientCollectionViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Ingredients) {
        }
    }
}