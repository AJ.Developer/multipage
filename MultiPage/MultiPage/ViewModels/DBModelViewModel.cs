﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.ViewModel;
using MultiPage.Localization;
using MultiPage.DBModelDataModel;

namespace MultiPage.ViewModels
{
    /// <summary>
    /// Represents the root POCO view model for the DBModel data model.
    /// </summary>
    public partial class DBModelViewModel : DocumentsViewModel<DBModelModuleDescription, IDBModelUnitOfWork>
    {

        const string TablesGroup = "Tables";

        const string ViewsGroup = "Views";

        /// <summary>
        /// Creates a new instance of DBModelViewModel as a POCO view model.
        /// </summary>
        public static DBModelViewModel Create()
        {
            return ViewModelSource.Create(() => new DBModelViewModel());
        }

        static DBModelViewModel()
        {
            MetadataLocator.Default = MetadataLocator.Create().AddMetadata<DBModelMetadataProvider>();
        }
        /// <summary>
        /// Initializes a new instance of the DBModelViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the DBModelViewModel type without the POCO proxy factory.
        /// </summary>
        protected DBModelViewModel()
            : base(UnitOfWorkSource.GetUnitOfWorkFactory())
        {
        }
        protected override void OnActiveModuleChanged(DBModelModuleDescription oldModule)
        {
            base.OnActiveModuleChanged(oldModule);
        }
        public override void OnClosing(CancelEventArgs cancelEventArgs)
        {
            base.OnClosing(cancelEventArgs);
        }
        protected override string GetModuleTitle(DBModelModuleDescription module)
        {
            return base.GetModuleTitle(module);
        }
   
        protected override DBModelModuleDescription[] CreateModules()
        {
            return new DBModelModuleDescription[] {
                new DBModelModuleDescription(DBModelResources.CategoryPlural, "CategoryCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Categories)),
                new DBModelModuleDescription(DBModelResources.ClientPlural, "ClientCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Clients)),
                new DBModelModuleDescription(DBModelResources.InvoicePlural, "InvoiceCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Invoices)),
                new DBModelModuleDescription(DBModelResources.InvoiceItemPlural, "InvoiceItemCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.InvoiceItems)),
                new DBModelModuleDescription(DBModelResources.SaleItemPlural, "SaleItemCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.SaleItems)),
                new DBModelModuleDescription(DBModelResources.SaleUnitPlural, "SaleUnitCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.SaleUnits)),
                new DBModelModuleDescription(DBModelResources.StockItemPlural, "StockItemCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.StockItems)),
                new DBModelModuleDescription(DBModelResources.IngredientUnitPlural, "IngredientUnitCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.IngredientUnits)),
                new DBModelModuleDescription(DBModelResources.IngredientPlural, "IngredientCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Ingredients)),
                new DBModelModuleDescription(DBModelResources.UserPlural, "UserCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Users)),
                new DBModelModuleDescription(DBModelResources.ConfigurationPlural, "ConfigurationCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Configurations)),
            };
        }
    }

    public partial class DBModelModuleDescription : ModuleDescription<DBModelModuleDescription>
    {
        public DBModelModuleDescription(string title, string documentType, string group, Func<DBModelModuleDescription, object> peekCollectionViewModelFactory = null)
            : base(title, documentType, group, peekCollectionViewModelFactory)
        {
        }
    }
}