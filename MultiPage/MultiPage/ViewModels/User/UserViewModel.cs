﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single User object view model.
    /// </summary>
    public partial class UserViewModel : SingleObjectViewModel<User, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of UserViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static UserViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new UserViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the UserViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the UserViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected UserViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Users, x => x.Username) {
                }


        /// <summary>
        /// The view model that contains a look-up collection of SaleItems for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<SaleItem> LookUpSaleItems {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (UserViewModel x) => x.LookUpSaleItems,
                    getRepositoryFunc: x => x.SaleItems);
            }
        }


        /// <summary>
        /// The view model for the UserSaleItems detail collection.
        /// </summary>
        public CollectionViewModelBase<SaleItem, SaleItem, long, IDBModelUnitOfWork> UserSaleItemsDetails {
            get {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (UserViewModel x) => x.UserSaleItemsDetails,
                    getRepositoryFunc: x => x.SaleItems,
                    foreignKeyExpression: x => x.UserID,
                    navigationExpression: x => x.User);
            }
        }
    }
}
