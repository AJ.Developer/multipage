﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single Client object view model.
    /// </summary>
    public partial class ClientViewModel : SingleObjectViewModel<Client, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of ClientViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ClientViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new ClientViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ClientViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ClientViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ClientViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Clients, x => x.Name) {
                }


        /// <summary>
        /// The view model that contains a look-up collection of Invoices for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<Invoice> LookUpInvoices {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (ClientViewModel x) => x.LookUpInvoices,
                    getRepositoryFunc: x => x.Invoices);
            }
        }


        /// <summary>
        /// The view model for the ClientInvoice detail collection.
        /// </summary>
        public CollectionViewModelBase<Invoice, Invoice, long, IDBModelUnitOfWork> ClientInvoiceDetails {
            get {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (ClientViewModel x) => x.ClientInvoiceDetails,
                    getRepositoryFunc: x => x.Invoices,
                    foreignKeyExpression: x => x.ClientID,
                    navigationExpression: x => x.Client);
            }
        }
    }
}
