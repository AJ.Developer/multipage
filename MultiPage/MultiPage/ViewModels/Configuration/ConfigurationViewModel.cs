﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single Configuration object view model.
    /// </summary>
    public partial class ConfigurationViewModel : SingleObjectViewModel<Configuration, int, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of ConfigurationViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ConfigurationViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new ConfigurationViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ConfigurationViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ConfigurationViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ConfigurationViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Configurations, x => x.Key) {
                }



    }
}
