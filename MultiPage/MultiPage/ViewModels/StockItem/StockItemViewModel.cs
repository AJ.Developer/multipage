﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single StockItem object view model.
    /// </summary>
    public partial class StockItemViewModel : SingleObjectViewModel<StockItem, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of StockItemViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static StockItemViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new StockItemViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the StockItemViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the StockItemViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected StockItemViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.StockItems, x => x.Name) {
                }


        /// <summary>
        /// The view model that contains a look-up collection of SaleUnits for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<SaleUnit> LookUpSaleUnits {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (StockItemViewModel x) => x.LookUpSaleUnits,
                    getRepositoryFunc: x => x.SaleUnits);
            }
        }
        /// <summary>
        /// The view model that contains a look-up collection of Categories for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<Category> LookUpCategories {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (StockItemViewModel x) => x.LookUpCategories,
                    getRepositoryFunc: x => x.Categories);
            }
        }
        /// <summary>
        /// The view model that contains a look-up collection of IngredientUnits for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<IngredientUnit> LookUpIngredientUnits {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (StockItemViewModel x) => x.LookUpIngredientUnits,
                    getRepositoryFunc: x => x.IngredientUnits);
            }
        }


        /// <summary>
        /// The view model for the StockItemSaleUnits detail collection.
        /// </summary>
        public CollectionViewModelBase<SaleUnit, SaleUnit, long, IDBModelUnitOfWork> StockItemSaleUnitsDetails {
            get {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (StockItemViewModel x) => x.StockItemSaleUnitsDetails,
                    getRepositoryFunc: x => x.SaleUnits,
                    foreignKeyExpression: x => x.StockItemID,
                    navigationExpression: x => x.StockItem);
            }
        }

        /// <summary>
        /// The view model for the StockItemIngredientUnits detail collection.
        /// </summary>
        public CollectionViewModelBase<IngredientUnit, IngredientUnit, long, IDBModelUnitOfWork> StockItemIngredientUnitsDetails {
            get {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (StockItemViewModel x) => x.StockItemIngredientUnitsDetails,
                    getRepositoryFunc: x => x.IngredientUnits,
                    foreignKeyExpression: x => x.StockItemID,
                    navigationExpression: x => x.StockItem);
            }
        }
    }
}
