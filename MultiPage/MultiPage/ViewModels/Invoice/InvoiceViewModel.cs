﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single Invoice object view model.
    /// </summary>
    public partial class InvoiceViewModel : SingleObjectViewModel<Invoice, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of InvoiceViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static InvoiceViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new InvoiceViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the InvoiceViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the InvoiceViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected InvoiceViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Invoices, x => x.FormattedDate) {
                }


        /// <summary>
        /// The view model that contains a look-up collection of Clients for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<Client> LookUpClients {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceViewModel x) => x.LookUpClients,
                    getRepositoryFunc: x => x.Clients);
            }
        }
        /// <summary>
        /// The view model that contains a look-up collection of InvoiceItems for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<InvoiceItem> LookUpInvoiceItems {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceViewModel x) => x.LookUpInvoiceItems,
                    getRepositoryFunc: x => x.InvoiceItems);
            }
        }


        /// <summary>
        /// The view model for the InvoiceInvoiceItems detail collection.
        /// </summary>
        public CollectionViewModelBase<InvoiceItem, InvoiceItem, long, IDBModelUnitOfWork> InvoiceInvoiceItemsDetails {
            get {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (InvoiceViewModel x) => x.InvoiceInvoiceItemsDetails,
                    getRepositoryFunc: x => x.InvoiceItems,
                    foreignKeyExpression: x => x.InvoiceID,
                    navigationExpression: x => x.Invoice);
            }
        }
    }
}
