﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single SaleItem object view model.
    /// </summary>
    public partial class SaleItemViewModel : SingleObjectViewModel<SaleItem, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of SaleItemViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static SaleItemViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new SaleItemViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the SaleItemViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the SaleItemViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected SaleItemViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.SaleItems, x => x.Name) {
                }


        /// <summary>
        /// The view model that contains a look-up collection of InvoiceItems for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<InvoiceItem> LookUpInvoiceItems {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (SaleItemViewModel x) => x.LookUpInvoiceItems,
                    getRepositoryFunc: x => x.InvoiceItems);
            }
        }
        /// <summary>
        /// The view model that contains a look-up collection of Categories for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<Category> LookUpCategories {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (SaleItemViewModel x) => x.LookUpCategories,
                    getRepositoryFunc: x => x.Categories);
            }
        }
        /// <summary>
        /// The view model that contains a look-up collection of Users for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<User> LookUpUsers {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (SaleItemViewModel x) => x.LookUpUsers,
                    getRepositoryFunc: x => x.Users);
            }
        }
        /// <summary>
        /// The view model that contains a look-up collection of SaleUnits for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<SaleUnit> LookUpSaleUnits {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (SaleItemViewModel x) => x.LookUpSaleUnits,
                    getRepositoryFunc: x => x.SaleUnits);
            }
        }


        /// <summary>
        /// The view model for the SaleItemInvoiceItems detail collection.
        /// </summary>
        public CollectionViewModelBase<InvoiceItem, InvoiceItem, long, IDBModelUnitOfWork> SaleItemInvoiceItemsDetails {
            get {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (SaleItemViewModel x) => x.SaleItemInvoiceItemsDetails,
                    getRepositoryFunc: x => x.InvoiceItems,
                    foreignKeyExpression: x => x.SaleItemID,
                    navigationExpression: x => x.SaleItem);
            }
        }

        /// <summary>
        /// The view model for the SaleItemSaleUnits detail collection.
        /// </summary>
        public CollectionViewModelBase<SaleUnit, SaleUnit, long, IDBModelUnitOfWork> SaleItemSaleUnitsDetails {
            get {
                return GetDetailsCollectionViewModel(
                    propertyExpression: (SaleItemViewModel x) => x.SaleItemSaleUnitsDetails,
                    getRepositoryFunc: x => x.SaleUnits,
                    foreignKeyExpression: x => x.SaleItemID,
                    navigationExpression: x => x.SaleItem);
            }
        }
    }
}
