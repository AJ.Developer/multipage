﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the SaleItems collection view model.
    /// </summary>
    public partial class SaleItemCollectionViewModel : CollectionViewModel<SaleItem, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of SaleItemCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static SaleItemCollectionViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new SaleItemCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the SaleItemCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the SaleItemCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected SaleItemCollectionViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.SaleItems) {
        }
    }
}