﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single InvoiceItem object view model.
    /// </summary>
    public partial class InvoiceItemViewModel : SingleObjectViewModel<InvoiceItem, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of InvoiceItemViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static InvoiceItemViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new InvoiceItemViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the InvoiceItemViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the InvoiceItemViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected InvoiceItemViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.InvoiceItems, x => x.InvoiceItemID) {
                }


        /// <summary>
        /// The view model that contains a look-up collection of Invoices for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<Invoice> LookUpInvoices {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceItemViewModel x) => x.LookUpInvoices,
                    getRepositoryFunc: x => x.Invoices);
            }
        }
        /// <summary>
        /// The view model that contains a look-up collection of SaleItems for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<SaleItem> LookUpSaleItems {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (InvoiceItemViewModel x) => x.LookUpSaleItems,
                    getRepositoryFunc: x => x.SaleItems);
            }
        }

    }
}
