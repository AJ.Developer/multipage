﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the InvoiceItems collection view model.
    /// </summary>
    public partial class InvoiceItemCollectionViewModel : CollectionViewModel<InvoiceItem, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of InvoiceItemCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static InvoiceItemCollectionViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new InvoiceItemCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the InvoiceItemCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the InvoiceItemCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected InvoiceItemCollectionViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.InvoiceItems) {
        }
    }
}