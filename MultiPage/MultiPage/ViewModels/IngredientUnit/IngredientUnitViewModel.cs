﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using MultiPage.DBModelDataModel;
using MultiPage.Common;
using MultiPage.DataLayer;

namespace MultiPage.ViewModels {

    /// <summary>
    /// Represents the single IngredientUnit object view model.
    /// </summary>
    public partial class IngredientUnitViewModel : SingleObjectViewModel<IngredientUnit, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of IngredientUnitViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static IngredientUnitViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new IngredientUnitViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the IngredientUnitViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the IngredientUnitViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected IngredientUnitViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.IngredientUnits, x => x.Error) {
                }


        /// <summary>
        /// The view model that contains a look-up collection of Ingredients for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<Ingredient> LookUpIngredients {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (IngredientUnitViewModel x) => x.LookUpIngredients,
                    getRepositoryFunc: x => x.Ingredients);
            }
        }
        /// <summary>
        /// The view model that contains a look-up collection of StockItems for the corresponding navigation property in the view.
        /// </summary>
        public IEntitiesViewModel<StockItem> LookUpStockItems {
            get {
                return GetLookUpEntitiesViewModel(
                    propertyExpression: (IngredientUnitViewModel x) => x.LookUpStockItems,
                    getRepositoryFunc: x => x.StockItems);
            }
        }

    }
}
